import React from 'react';
import Home from './pages/Home';

import './App.css';
import './styles/style.css';

function App() {
  return (
    <div className="App">
      <Home />
    </div>
  );
}

export default App;
