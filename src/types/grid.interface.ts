export interface IAvailable{
	available: number
}

export interface IMovie {
	id: number;
	name: string;
	description: string;
	thumbnail: {path: string, extension: string};
	comics: IAvailable;
}

export interface IGrid {
	items: Array<IMovie>
}

export interface IGridItem {
	item: IMovie
}
