import { IMovie } from './grid.interface';

export interface IHomeState {
	searchText: string;
	results: Array<IMovie>,
	isApiCalled: boolean
}

export interface IHomeProps {

}
