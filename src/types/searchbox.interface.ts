export interface ISearchBox {
	searchText: string
	handleChange: (e: any) => Promise<void>
}
