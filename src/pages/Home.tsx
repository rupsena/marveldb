import React, { Component } from 'react';

import { IHomeProps, IHomeState } from '../types/home.interface';
import { IMovie } from '../types/grid.interface';

import SearhBox from '../components/SeachBox';
import Grid from '../components/Grid';

const BASEURL: string = 'https://gateway.marvel.com/v1/public/characters';
const APICREDS: string = '&apikey=6871af630ca51742153d2db8dbf10dcb&hash=609bb5211dbf0e54c53e927bb92f5ee7&ts=1';

let timer: NodeJS.Timeout | null = null;

class Home extends Component<IHomeProps, IHomeState> {
	constructor(props: IHomeProps) {
		super(props);
		this.state={
			searchText: '',
			results: [],
			isApiCalled: false
		}
	}

	onHandleChange = async (e: any): Promise<void> => {
		const { value } = e.target;
		await this.setState({searchText: value});
		if (timer) {
			clearTimeout(timer);
		}
		// Debouncing
		timer = setTimeout(async() => {
			if (this.state.searchText) {
				await this.search();
			} else {
				await this.setState({results: []});
			}
		}, 500);
	}

	search = async () => {
		const url: string = `${BASEURL}?nameStartsWith=${this.state.searchText}${APICREDS}`;
		await this.setState({isApiCalled: true});
		const response = await fetch(url, {
			method: 'GET'
		});
		const data = await response.json();
		let results: Array<IMovie> = [];
		if (data.status === 'Ok' && data.code === 200 && data.data?.count) {
			results = data.data.results.map((item: IMovie) => {
				return {
					id: item.id,
					name: item.name,
					description: item.description,
					thumbnail: {path: item.thumbnail.path, extension: item.thumbnail.extension},
					comics: item.comics.available
				}
			});
		}

		await this.setState({results, isApiCalled: false})
	}

	render () {
		return (
			<div>
				<header>
					<h2 data-testid="homeheader">Marvel DB</h2>
				</header>
				<SearhBox searchText={this.state.searchText} handleChange={this.onHandleChange}/>
				<Grid items={this.state.results}/>
			</div>
		)
	}
}

export default Home;
