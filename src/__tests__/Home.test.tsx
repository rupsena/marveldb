import Home from '../pages/Home';
import { render, screen, fireEvent } from '@testing-library/react';

describe('renders Home', () => {
	test('renders Home with data and match snapshot', () => {
		render(<Home/>);
		const homeComp = screen.getAllByText('Marvel DB');
		expect(homeComp).toMatchSnapshot();
	});

	test('renders Home and test searchbox functionality', async () => {
		render(<Home/>);
		const homeComp = screen.getByTestId('searchText');
		const searchBox = screen.getByTestId('searchbox');

		expect(homeComp).toBeInTheDocument();

		await fireEvent.change(searchBox, {target: {value: 'spider'}});
		await fireEvent.change(searchBox, {target: {value: 'iron'}});
		const homeCompUpdated = screen.getByTestId('searchText');
		expect(homeCompUpdated).toHaveTextContent('iron');

		await fireEvent.change(searchBox, {target: {value: 'spider'}});
		await fireEvent.change(searchBox, {target: {value: ''}});
		const searchCleared = screen.getByTestId('searchText');
		expect(searchCleared).toHaveTextContent('No search criteria');


	});
})
