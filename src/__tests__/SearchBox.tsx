import SearhBox from '../components/SeachBox';
import { render, screen } from '@testing-library/react';

describe('renders SearhBox', () => {
	test('renders SearhBox with data and match snapshot', () => {
		render(<SearhBox searchText={'spider'} handleChange={async (e: any): Promise<void> => {}}/>);
		const searchBox = screen.getByTestId('searchbox');
		expect(searchBox).toMatchSnapshot();
	});
	test('renders with props', () => {
		render(<><SearhBox searchText={'spider'} handleChange={async (e: any): Promise<void> => {}}/></>);
		const searchBox = screen.getByTestId('searchbox');

		expect(searchBox).toBeInTheDocument();
	});
})

