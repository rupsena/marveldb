import Grid from '../components/Grid';
import { render, screen } from '@testing-library/react';
import { IMovie } from '../types/grid.interface';

const MOCK_DATA: Array<IMovie> = [{
	id: 1010727,
	name: 'Spider-Girl (May Parker)',
	description: 'May "Mayday" Parker is the daughter of Spider-Man and Mary Jane Watson-Parker. Born with all her fatherï¿½s powers-and the same silly sense of humor-sheï¿½s grown up to become one of Earthï¿½s most trusted heroes and a fitting tribute to her proud papa.',
	thumbnail: {path: 'http://i.annihil.us/u/prod/marvel/i/mg/1/70/4c003adccbe4f', extension: 'jpg'},
	comics: {available: 196}
}];

describe('renders Grid', () => {
	test('renders grid and match snapshot', () => {
		render(<><Grid items={MOCK_DATA}/></>);
		const gridComp = screen.getAllByText('Spider-Girl (May Parker)');
		expect(gridComp).toMatchSnapshot();
	});

	test('renders with Grid without data', () => {
		render(<Grid items={[]}/>);
		const gridComp = screen.getAllByText('No results found');

		expect(gridComp).toBeTruthy();
	});
})
