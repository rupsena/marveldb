import GridItem from '../components/GridItem';
import { render, screen } from '@testing-library/react';

const MOCK_DATA: {
	id: number;
	name: string;
	description: string;
	thumbnail: {path: string, extension: string};
	comics: number;
} = {
	id: 1010727,
	name: 'Spider-Girl (May Parker)',
	description: 'May "Mayday" Parker is the daughter of Spider-Man and Mary Jane Watson-Parker. Born with all her fatherï¿½s powers-and the same silly sense of humor-sheï¿½s grown up to become one of Earthï¿½s most trusted heroes and a fitting tribute to her proud papa.',
	thumbnail: {path: 'http://i.annihil.us/u/prod/marvel/i/mg/1/70/4c003adccbe4f', extension: 'jpg'},
	comics: 196
}

describe('renders GridItem', () => {
	test('renders SearhBox with data and match snapshot', () => {
		render(<GridItem item={MOCK_DATA}/>);
		const gridItemName = screen.getByTestId('name');
		const gridItemDescription= screen.getByTestId('description');
		const gridItemComics= screen.getByTestId('comics');
		const gridItemThumbnail= screen.getByTestId('thumbnail');

		expect(gridItemName).toMatchSnapshot();
		expect(gridItemDescription).toMatchSnapshot();
		expect(gridItemComics).toMatchSnapshot();
		expect(gridItemThumbnail).toMatchSnapshot();
	});

	test('renders GridItem component with mock data', () => {
		render(<GridItem item={MOCK_DATA}/>);
		const gridItemName= screen.getByTestId('name');
		const gridItemDescription= screen.getByTestId('description');
		const gridItemComics= screen.getByTestId('comics');
		const gridItemThumbnail= screen.getByTestId('thumbnail');

		expect(gridItemName).toBeInTheDocument();
		expect(gridItemDescription).toBeInTheDocument();
		expect(gridItemComics).toBeInTheDocument();
		expect(gridItemThumbnail).toBeInTheDocument();
	});

	test('checks whether proper data is passed as props', async () => {
		render(<><GridItem item={MOCK_DATA}/></>);
		const gridItemName= screen.getByText(MOCK_DATA.name);
		const gridItemDescription= screen.getByText(MOCK_DATA.description);
		const gridItemComics= screen.getByText('Published comics: 196');

		expect(gridItemName).toBeInTheDocument();
		expect(gridItemDescription).toBeInTheDocument();
		expect(gridItemComics).toBeInTheDocument();
	});

	test('checks whether components render without description', async () => {
		render(<><GridItem item={{...MOCK_DATA, description: ''}}/></>);
		const gridItemName= screen.getByText(MOCK_DATA.name);
		const gridItemDescription= screen.getByText('This movie does not contain sufficient content');
		const gridItemComics= screen.getByText('Published comics: 196');

		expect(gridItemName).toBeInTheDocument();
		expect(gridItemDescription).toBeInTheDocument();
		expect(gridItemComics).toBeInTheDocument();
	});
})

