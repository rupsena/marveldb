import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
  render(<App />);
  const appHeader = screen.getAllByText('Marvel DB');;
  expect(appHeader).toMatchSnapshot();
});

