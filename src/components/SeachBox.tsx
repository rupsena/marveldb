import { ISearchBox } from '../types/searchbox.interface';

function SearhBox(props: ISearchBox) {
  return (
    <div className="searchbox">
      <input type="text" name="search" value={props.searchText} onChange={props.handleChange} data-testid="searchbox"/>
	  <p>
	  	Results for "<span data-testid="searchText" className="search-criteria">{props.searchText || 'No search criteria'}</span>"
	  </p>
    </div>
  );
}

export default SearhBox;
