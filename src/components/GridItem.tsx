import { IGridItem } from '../types/grid.interface';

function GridItem(props: IGridItem) {
  return (
	<div className="item">
		<div className="card-body">
			<div className="image">
				<img
				src={`${props.item.thumbnail.path}.${props.item.thumbnail.extension}`}
					alt={props.item.name}
					data-testid="thumbnail"
					height="200" width="250"/>
			</div>
			<h3 data-testid="name">{props.item.name}</h3>
			<p data-testid="description">{props.item.description || 'This movie does not contain sufficient content'}</p>
		</div>
		<div className="comic-count">
			<label data-testid="comics">{`Published comics: ${props.item.comics}`}</label>
		</div>
	</div>
  );
}

export default GridItem;
