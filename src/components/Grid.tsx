import { IGrid, IMovie } from '../types/grid.interface';

import GridItem from './GridItem';

function Grid(props: IGrid) {
  return (
    <div>
		{props.items.length > 0 ? (
			<div className="grid-item">{props.items.map((item: IMovie, i: number) => {
				return (
					<GridItem item={item} key={`movie-item-${i}`}/>
				)
			})}</div>) : <p>No results found</p>}
    </div>
  );
}

export default Grid;
